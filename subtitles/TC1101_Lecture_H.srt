1
00:00:00,240 --> 00:00:07,000
Welcome to this OST2 course, to learn how to use 
the built-in cryptography functions of the TPM. 

2
00:00:07,000 --> 00:00:13,040
By using private material that never leaves the 
TPM we can perform encryption over our sensitive  

3
00:00:13,040 --> 00:00:20,800
data. This provides us with very high guarantees. 
The TPM has built-in commands for RSA encryption  

4
00:00:20,800 --> 00:00:27,560
and symmetric encryption. However we don't have 
at this time commands for ECC encryption. We can  

5
00:00:27,560 --> 00:00:34,520
receive support for ECC encryption TPM commands 
with a firmware update in the future. To perform  

6
00:00:34,520 --> 00:00:43,400
symmetric encryption we would need naturally a TPM 
key of symmetric type, usually 128bit AES. Remember  

7
00:00:43,400 --> 00:00:49,760
that symmetric algorithms are regulated and there are 
export restrictions. To check the capabilities of  

8
00:00:49,760 --> 00:00:56,920
the TPM you have we have a separate lecture. We 
can see what algorithms and commands are supported.  

9
00:00:56,920 --> 00:01:06,480
Your TPM could support 192bit or 256bit AES or 
a different symmetric algorithm. Using the  

10
00:01:06,480 --> 00:01:14,000
TPM2_EncryptDecrypt2() we can use our child key 
of symmetric type and decrypt input data, either  

11
00:01:14,000 --> 00:01:20,640
from the standard input or from a file as a last 
argument to the tool. The output is stored by a  

12
00:01:20,640 --> 00:01:27,360
file pointed to us using the -o switch. Using the 
same tool and just adding the -d switch we  

13
00:01:27,360 --> 00:01:35,080
are instructing the TPM to decrypt our data. Having 
the built-in symmetric encryption capability we're  

14
00:01:35,080 --> 00:01:43,240
able to have a workaround and make use of the 
ECC encryption that TPM supports. We create a  

15
00:01:43,240 --> 00:01:50,880
primary key of ECC type and then generate a child 
key that is going to be wrapped in this primary  

16
00:01:50,880 --> 00:01:58,480
key. This is not the same thing as using native 
ECC encryption over our data but it has become  

17
00:01:58,480 --> 00:02:04,280
something of an industry practice. And because 
symmetric encryption is usually quite fast and  

18
00:02:04,280 --> 00:02:11,240
also ECC is very friendly to memory constrained 
systems we see this approach being used on bare  

19
00:02:11,240 --> 00:02:18,680
metal embedded systems a lot. The TPM has built-in 
commands for RSA encryption this is achieved using  

20
00:02:18,680 --> 00:02:28,160
two tools, the tpm2_rsaencrypt and the tpm2_rsadecrypt.  
 Notice that the tool can take the padding  

21
00:02:28,160 --> 00:02:34,480
schemes from the TPM key. This information is 
set when the key was created. The tool can do  

22
00:02:34,480 --> 00:02:43,840
this but by default it relies on using PKCS v1.5 
padding scheme. This is an important detail.

23
00:02:43,840 --> 00:02:50,040
The other options are familiar. We have a -c for the key 
that we're going to use we have a -o for storing  

24
00:02:50,040 --> 00:02:56,320
the encrypted data. This tool does not support 
parameter encryption. The tpm2_rsadecrypt  

25
00:02:56,320 --> 00:03:04,840
takes in our encrypted data and can have optional 
authorization using the -p switch. This allows us  

26
00:03:04,840 --> 00:03:10,760
to provide session for parameter encryption. The 
rest of the options are quite familiar. Here's an  

27
00:03:10,760 --> 00:03:16,720
example. The first example does not use parameter 
encryption. Once we have our data we execute the  

28
00:03:16,720 --> 00:03:24,520
two tools and we do the forward operation and the 
reverse operation. The second example starts a HMAC  

29
00:03:24,520 --> 00:03:30,880
session for parameter encryption and feeds it to 
the tpm2_rsadecrypt tool. As you can see we need  

30
00:03:30,880 --> 00:03:38,360
to specify that we are feeding a session context. 
Using RSA encryption on the TPM can be rather slow.  

31
00:03:38,360 --> 00:03:45,120
This is why I would recommend using an ECC primary 
key and the symmetric encryption we saw earlier.

32
00:03:45,120 --> 00:03:51,120
If you have a question please write to us in the OST2 
discussion board after each unit this helps us  

33
00:03:51,120 --> 00:03:57,240
understand better the context of your question. 
Also feel free to email us at the course email.

